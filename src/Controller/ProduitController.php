<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Repository\CategorieRepository;

use App\Repository\ProduitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ProduitType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class ProduitController extends AbstractController
{
    #[Route('/produit', name: 'app_produit', methods: ['GET','POST'])]
    public function index(ProduitRepository $repository,EntityManagerInterface $manager,CategorieRepository $repoCategorie,Request $request): Response
    {
        $produits = $repository->findAll();

        if( $request->request->get('categ') && $request->request->get('categ') != 0){
             
            $categ = $request->request->get('categ');

            $produits = $repository->parCateg($categ);
        }

        $categories = $repoCategorie->findAll();

        // $produits = $query->getResult();
        // dd($produits);
        // dd($produits);
        return $this->render('produit/index.html.twig', [
            'produits' => $produits,
            'categories' => $categories
        ]);
    }


    #[Route('/produit/creer', name: 'app_produit.create', methods: ['GET', 'POST'])]

    public function new(Request $request,EntityManagerInterface $manager) : Response 
    {
        $produit = new Produit();
        $form = $this->createForm(ProduitType::class, $produit);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $produit = $form->getData();

            $manager->persist($produit);
            $manager->flush();

            $this->addFlash(
                'success',
                'Le produit est ajouté  !'
            );


            return $this->redirectToRoute('app_produit');
        }

        return $this->render('produit/create.html.twig',[
                  "form" => $form->createView() 
        ]);
    }

    #[Route('/produit/modifier/{id}', name: 'app_produit.edit', methods: ['GET', 'POST'])]

    public function edit( Produit $produit, Request $request, EntityManagerInterface $manager): Response
    {

        $form = $this->createForm(ProduitType::class, $produit);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $produit = $form->getData();

            $manager->persist($produit);
            $manager->flush();

            $this->addFlash(
                'success',
                'Le produit est modifié  !'
            );


            return $this->redirectToRoute('app_produit');
        }
        return $this->render('produit/edit.html.twig', [
            'form' => $form->createView()
        ]);

    }


    #[Route('/produit/supprimer/{id}', name: 'app_produit.delete', methods: ['GET'])]

    public function delete(EntityManagerInterface $manager,Produit $produit) : Response 
    {

        $manager->remove($produit);
        $manager->flush();

        $this->addFlash(
            'success',
            'Le produit est supprimé  !'
        );

        return $this->redirectToRoute('app_produit');
    }
}

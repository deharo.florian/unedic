<?php

namespace App\Form;

use App\Entity\Produit;
use App\Entity\Categorie;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use  App\Repository\CategorieRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Assert ;

class ProduitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom',TextType::class,[
                'attr' => [
                    'class' => 'form-control',
                    'minlenght' => '2',
                    'maxlenght' => '255'
                ],
                'label' => 'Nom',
                'constraints' => [
                    new Assert\Length(['min' => 2, 'max' => 255]),
                    new Assert\NotBlank()
                ]
                ])
            ->add('prix',MoneyType::class,[
                'attr' => [
                    'class' => 'form-control'
                ],
                'label' => 'Prix en ',
                'constraints' => [
                        new Assert\Positive()
                ]
                ])
            ->add('description',TextareaType::class,[
                'attr' => [
                    'class' => 'form-control',
                    'minlenght' => '2',
                    'maxlenght' => '300'
                ],
                'label' => 'Description',
                'constraints' => [
                        new Assert\Length(['min' => 2, 'max' => 300]),
                ]
                ])
            ->add('categorie',EntityType::class, [
                'class' => Categorie::class,
                'attr' => [
                    'class' => 'form-control'
                ],
                'label' => 'Categorie',
                'required' => false,
                'query_builder' => function (CategorieRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.nom', 'ASC');
                },
                    ])
            ->add("submit",SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-outline-info my-3'
                ],
                'label' => 'Valider'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Produit::class,
        ]);
    }
}

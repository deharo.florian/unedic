<?php

namespace App\DataFixtures;

use App\Entity\Categorie;
use App\Entity\Produit;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $categ1 = new Categorie();
        $categ1->setNom("ecran");

        $categ2 = new Categorie();
        $categ2->setNom("clavier");

        $categ3 = new Categorie();
        $categ3->setNom("souris");

        $produit1 = new Produit();
        $produit1->setNom("Souris Logitech LOGITECH SANS FIL COMBO MK545")
                    ->setPrix(70)
                    ->setDescription("Expérience de frappe instantanément familière. Une souris de pointe de taille standard pour un confort et un contrôle parfaits
                    Fiable et tout en simplicité, offrant une autonomie longue durée
                    Liberté du sans fil grâce à un récepteur USB Unifying™ compact
                ")
                ->setCategorie($categ3);

        $manager->persist($categ1);
        $manager->persist($categ2);
        $manager->persist($categ3);
        $manager->persist($produit1);

        $manager->flush();
    }
}
